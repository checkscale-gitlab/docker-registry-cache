# Docker Registry Cache

Docker Compose project that setups up a local docker.io pull through cache

## System Requirements
### Required
- docker
- docker-compose

### Optional
- ifconfig (shows host IP for adding to host files)

## Install

1. Clone this repo `git clone https://gitlab.com/fluentops/gizmos/docker-registry-cache.git registry-cache`
2. Copy `.env.template` to `.env`
3. Copy `docker-compose.yml.template` to `docker-compose.yml`
4. Edit `.env` as per preference
5. Run `./install.sh`
6. Run `docker-compose up -d`
7. Distribute the `registry-ca.crt` or add cache as an insecure registry in the `daemon.json` config file
8. Update `/etc/docker/daemon.json` with `registry-mirrors` settings
9. Update `/etc/hosts` or DNS with registry dns
